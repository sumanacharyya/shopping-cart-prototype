import React, {useState} from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import "react-toastify/dist/ReactToastify.css";
import './App.css';
import { Container, Row, Col } from 'reactstrap';

import { ToastContainer, toast } from 'react-toastify';
import BuyPage from './components/BuyPage';
import Cart from './components/Cart';


function App() {

  const [cartItem, setCartItem] = useState([]);


  const addCartHandler = (item) => {
    const isAlreadyAdded = cartItem.findIndex((arr) => {
      return arr.id === item.id;
    });

    if (isAlreadyAdded !== -1){
      toast("Already is the cart!", {
        type: "error",
      });
    }
    else {
      setCartItem([...cartItem, item]);
    }
  };

  const buyNowHandler = () => {
    setCartItem([]);  
    toast("Purchase Complete", {
      type: "success",
    })  
  };

  const removeItemsHandler = (item) => {
    setCartItem(cartItem.filter(singleItem => singleItem.id !== item.id));
  };

  return (
    <Container fluid>
      <ToastContainer />
      <Row>
        <Col md="8">
          <BuyPage addInCart={addCartHandler} />
        </Col>
        <Col md="4">
          <Cart cartItem={cartItem} buyNow={buyNowHandler} removeItem={removeItemsHandler}/>
        </Col>
      </Row>
    </Container>
  );
}

export default App;
