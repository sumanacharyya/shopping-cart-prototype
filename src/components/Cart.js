import React from "react";
import {
  Container,
  ListGroup,
  ListGroupItem,
  Button,
  CardHeader,
  CardBody,
  Card,
  CardFooter,
  Col,
  Row,
} from "reactstrap";

const Cart = ({cartItem, buyNow, removeItem}) => {
    let amount = 0;
    cartItem.forEach((eachItem) => {
        amount = parseFloat(amount) + parseFloat(eachItem.productPrice);
    });

  return (
        <Container fluid>
            <h1 className="text-success">Your Cart</h1>
            <ListGroup>
                {
                    cartItem.map((eachItem) => (
                        <ListGroupItem key={eachItem.id}>
                            <Row>
                                <Col>
                                    <img 
                                        height={80}
                                        src={eachItem.tinyImage}
                                        alt="TinyImage"
                                    />
                                </Col>
                                <Col className="text-center">
                                    <div className="text-primary">
                                        {eachItem.productName}
                                    </div>
                                    <span>Price: RS - {eachItem.productPrice}</span>
                                    <Button color="danger" onClick={() => removeItem(eachItem)}>Remove</Button>
                                </Col>
                            </Row>
                        </ListGroupItem>
                    ))
                }
            </ListGroup>
            {/* If Everything is empty */}
            {
                cartItem.length > 0 ? (
                    <Card className="text-center mt-3">
                        <CardHeader>
                            Grand Total: 
                        </CardHeader>
                        <CardBody>
                            Your Amount for {cartItem.length} product is {amount}
                        </CardBody>
                        <CardFooter>
                            <Button color="success" onClick={buyNow}>Buy Now</Button>
                        </CardFooter>
                    </Card>
                ) : (
                    <h1 className="text-warning">Cart Is Empty</h1>
                )
            }
        </Container>
    )
};

export default Cart;
