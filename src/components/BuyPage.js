import React, { useEffect, useState } from 'react'
import Axios from 'axios';
import { faker } from '@faker-js/faker';
// import { Commerce } from '@faker-js/faker/modules/commerce';
import { Container, Col, Row } from 'reactstrap';
import CartItem from './CartItem';

const url = "https://api.npoint.io/8522bd87364deb38244c";  // https://www.npoint.io/docs/8522bd87364deb38244c

const BuyPage = ({addInCart}) => {
    const [products, setProducts] = useState([]);
    
    
    const fetchPhotoesHandler = async () => {
        const {data} = await Axios.get(url)
        const {photos} = data;
        const allProduct = photos.map((eachPhoto) => ({
            smallImage: eachPhoto.src.medium,
            tinyImage: eachPhoto.src.tiny,
            productName: faker.random.word(),
            productPrice: faker.commerce.price(),
            id: faker.datatype.uuid(),
        }));
        setProducts(allProduct);
    }

    useEffect(() => {
        fetchPhotoesHandler();
    }, []);
    

  return (
    <Container fluid>
        <h1 className='text-success text-center'>
            Buy Page
        </h1>
        <Row>
            {
                products.map((eachProduct) => (
                    <Col md={4} key={eachProduct.id}>
                        <CartItem product={eachProduct} addInCart={addInCart}/>
                    </Col>
                ))
            }
        </Row>
    </Container>
  )
}

export default BuyPage